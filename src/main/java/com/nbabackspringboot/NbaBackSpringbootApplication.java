package com.nbabackspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NbaBackSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(NbaBackSpringbootApplication.class, args);
    }

}
