package com.nbabackspringboot.Repository;

import com.nbabackspringboot.Model.ScoreEquipeModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ScoreEquipeRepository extends JpaRepository<ScoreEquipeModel,String> {
    @Query(value = "SELECT idmatch,idequipe, total_points FROM totalpointbymatch WHERE idmatch = :idmatch", nativeQuery = true)
    List<ScoreEquipeModel> findByCustomQuery(@Param("idmatch") int idmatch);
}
