package com.nbabackspringboot.Repository;

import com.nbabackspringboot.Model.EquipeModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipeRepository extends JpaRepository<EquipeModel, String> {
    EquipeModel findByIdequipe(int idequipe);
}
