package com.nbabackspringboot.Repository;

import com.nbabackspringboot.Model.JoueurModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JoueurRepository extends JpaRepository<JoueurModel, String> {

}
