package com.nbabackspringboot.Repository;

import com.nbabackspringboot.Model.MatchModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatchRepository extends JpaRepository<MatchModel, String> {
    MatchModel findByIdmatch(int idmatch);
}
