package com.nbabackspringboot.Model;


import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.util.List;


@Entity
public class ScoreEquipeModel {


    int idmatch;
    @Id
    int idequipe;
    int total_points;

    public int getIdmatch() {
        return idmatch;
    }

    public void setIdmatch(int idmatch) {
        this.idmatch = idmatch;
    }

    public int getIdequipe() {
        return idequipe;
    }

    public void setIdequipe(int idequipe) {
        this.idequipe = idequipe;
    }

    public int getTotal_points() {
        return total_points;
    }

    public void setTotal_points(int total_point) {
        this.total_points = total_point;
    }
}
