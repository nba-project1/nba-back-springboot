package com.nbabackspringboot.Model;

import jakarta.persistence.*;

@Entity
@Table(name="joueur")
public class JoueurModel {
    @Id
    int idjoueur;
    @ManyToOne
    @JoinColumn(name = "idequipe")
    EquipeModel equipe;
    String nom;
    String prenoms;

    public int getIdjoueur() {
        return idjoueur;
    }

    public void setIdjoueur(int idjoueur) {
        this.idjoueur = idjoueur;
    }

    public EquipeModel getEquipe() {
        return equipe;
    }

    public void setEquipe(EquipeModel equipe) {
        this.equipe = equipe;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenoms() {
        return prenoms;
    }

    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }
}
