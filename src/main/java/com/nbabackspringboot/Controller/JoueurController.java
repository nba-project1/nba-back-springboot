package com.nbabackspringboot.Controller;

import com.nbabackspringboot.Model.JoueurModel;
import com.nbabackspringboot.Repository.JoueurRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/joueur")
public class JoueurController {
    private final JoueurRepository joueurRepository;

    public JoueurController(JoueurRepository joueurRepository) {
        this.joueurRepository = joueurRepository;
    }

    @GetMapping("all")
    public List<JoueurModel> getAllJoueur(){
        return joueurRepository.findAll();
    }
}
