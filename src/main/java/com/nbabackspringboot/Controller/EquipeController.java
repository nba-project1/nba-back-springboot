package com.nbabackspringboot.Controller;

import com.nbabackspringboot.Model.EquipeModel;
import com.nbabackspringboot.Repository.EquipeRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/equipe")
public class EquipeController {
    private final EquipeRepository equipeRepository;

    public EquipeController(EquipeRepository equipeRepository) {
        this.equipeRepository = equipeRepository;
    }

    @GetMapping("by-id/{idequipe}")
    public EquipeModel getEquipById(@PathVariable int idequipe){
        return equipeRepository.findByIdequipe(idequipe);
    }
}
