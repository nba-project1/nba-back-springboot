package com.nbabackspringboot.Controller;


import com.nbabackspringboot.Model.MatchModel;
import com.nbabackspringboot.Repository.MatchRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/match")
public class MatchController {
    private final MatchRepository matchRepository;

    public MatchController(MatchRepository matchRepository) {
        this.matchRepository = matchRepository;
    }

    @GetMapping("by-id/{idmatch}")
    public MatchModel findById(@PathVariable int idmatch){
        return matchRepository.findByIdmatch(idmatch);
    }
}
