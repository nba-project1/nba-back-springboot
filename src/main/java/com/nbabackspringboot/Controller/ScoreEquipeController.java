package com.nbabackspringboot.Controller;


import com.nbabackspringboot.Model.ScoreEquipeModel;
import com.nbabackspringboot.Repository.ScoreEquipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/scoreEquipe")
public class ScoreEquipeController {
    private final ScoreEquipeRepository scoreEquipeRepository;

    public ScoreEquipeController(ScoreEquipeRepository scoreEquipeRepository) {
        this.scoreEquipeRepository = scoreEquipeRepository;
    }

    @GetMapping("idmatch/{idmatch}")
    public List<ScoreEquipeModel> getScoreEquipeByIdMatch(@PathVariable int idmatch){
        return scoreEquipeRepository.findByCustomQuery(idmatch);
    }
}
